use core::panic;
use std::{fmt::Debug, fs::File, io::{Write, self, BufRead}};
use md5;
use rusqlite::{Connection, OpenFlags};
use regex::Regex;
use std::fs;
use serde::{Serialize, Deserialize};
use dirs::home_dir;
//use std::path::Path;
//use std::env;
use std::process::Command;
// version 1.0
/*
	primary:
		- load database
		- identify rows
		- identify header toml (!)
		- identify images
		- copy images to location
		- copy the content to location
		- config file

	code cleanup
		- move all struct to other files
*/



#[derive(Debug)]
#[allow(dead_code)]
struct TableItemChanges{
    id: u32,
    item_type: u32,
    item_id: String,
    type1: u32,
    created_time:u64,
    source: u32,
    before_change_item: String,
}

#[derive(Debug)]
#[allow(dead_code)]
struct TableNotes {
	id: String,
 	parent_id : String,
	title: String,
	body: String,
	markup_language: u64,
}


#[derive(Deserialize, Serialize)]
struct Config {
	dbpath: String,
	image_src_path: String,
	image_dest_path: String,
	posts_path: String,
	posts_doc_path : String,
	auto_push : bool

}

#[allow(dead_code)]
struct ImageFilename{
	original_filename: String,
	source_extension: String,
	source_filename : String,	
}

fn main() {
	let exit_code: i32 = shadow_main();
	std::process::exit(exit_code as i32);
}

fn get_config() -> Option<Config> {
	//let config_filename = "~/.joplin2hugo.conf";


	//needed to construct the full path
	let full_config_path = match home_dir() {
		Some(v) => v,
		None => {
			eprintln!("unable to get home directory");
			return None;
		},
	};


	//let path = full_config_path.to_str().clone().to_ow .(config_filename.clone());
	
	let config_filename = format!("{}/.joplin2hugo.toml", full_config_path.display());
	//println!("{:?}", config_filename);
	//return None;


	let mut configuration: Config = Config { dbpath: "".to_owned(), image_src_path: "".to_owned(), image_dest_path: "".to_owned(), posts_path: "".to_owned(), auto_push: false, posts_doc_path: "".to_owned() };

	// if exists.. read it 
	if std::path::Path::new(&config_filename).exists() {
		let contents = match fs::read_to_string(config_filename) {
			Ok(v) => v,
			Err(..) => {
				eprintln!("[ERR] Should have been able to read the config file");
				return None;
			}
		};
		
		//println!("{:?}", contents);
		//configuration = toml::from_str(contents.as_str()).unwrap();
		configuration = match toml::from_str(contents.as_str()) {
			Ok(v) => v,
			Err(..) => {
				eprintln!("[ERR] Unable to parse the configuration file...");
				return None;
			}
		}
	}
	else
	{
		println!("INITIAL SETUP");
		println!("------------------------");
		// check dbpath
		loop {
			print!("Enter Joplin database path: ");
			io::stdout().flush().unwrap();
			let stdin = io::stdin();
			let value = stdin.lock().lines().next().unwrap().unwrap().trim().to_string();
			let pth = format!("{}/database.sqlite", value);
			if std::path::Path::new(&pth).is_file() {
				configuration.dbpath = pth;
				configuration.image_src_path = format!("{}/resources/",value).to_string();
				break;
			}
			println!("[ERR] no such file found.. (Use CTRL + C to quit)");
		}
		
		// check image_src_path
	
		loop {
			print!("Enter hugo repo path: ");
			io::stdout().flush().unwrap();
			let stdin = io::stdin();
			let value = stdin.lock().lines().next().unwrap().unwrap().trim().to_string();
			if std::path::Path::new(&value).exists() && !std::path::Path::new(&value).is_file() {
				configuration.image_dest_path = format!("{}static/", &value);
				configuration.posts_path = format!("{}content/posts/", &value);
				configuration.posts_doc_path = value.clone();

				if std::path::Path::new(&configuration.image_dest_path).exists() && !std::path::Path::new(&configuration.image_dest_path).is_file() {
					println!("[INFO] hugo static folder found..");
				}
				else {
				    panic!("[ERR cannot continue.. no static folder found");
				}

				if std::path::Path::new(&configuration.posts_path).exists() && !std::path::Path::new(&configuration.posts_path).is_file() {
					println!("[INFO] hugo posts folder found..");
				}
				else {
					panic!("[ERR cannot continue.. no content/posts folder found");
				}
				break;
			}
			println!("[ERR] no such folder found.. (Use CTRL + C to quit)");
		}

		loop {
			print!("Would you like to autopush after each sync (y/n)");
			io::stdout().flush().unwrap();
			let stdin = io::stdin();
			let value = stdin.lock().lines().next().unwrap().unwrap().trim().to_string();
			if value == "Y" || value == "y" {
				configuration.auto_push = true;
				break;
			}
			else if value == "N" || value == "n" {
				configuration.auto_push = false;
			}
			else {
				print!("Invalid value.. please re-enter");
				continue;
			}
		}

		let encoded = toml::to_string(&configuration).unwrap();
		let mut f = File::create(config_filename).expect("[ERR] unable to create configuration file");
		f.write_all(encoded.as_bytes()).expect("[ERR] unable to config to file");
		println!("[INFO] configuration settings has been written...");	
		println!("[INFO] please rerun the command...");	
		return None;
	}

	
	// // if perform_auto_push(configuration.posts_doc_path) == false
	// // {
	// // 	return None;
	// // }
	
	// when its time to save
	//let decoded: Config = toml::from_str(toml_str).unwrap();
	return Some(configuration);
	
}
fn check_id(hugo_path: String, post_id: String) -> Option<(bool, String)> {
	let files = match fs::read_dir(hugo_path) {
		Ok(v ) => v,
		Err(e) => {
			eprintln!("[ERR] unable to read files : \n {}", e.to_string());
			return None;
		},
	};
	let re = Regex::new(r#"([a-zA-Z0-9]{32})-([a-zA-Z0-9]*.md)"#).unwrap();
	for file in files {
		//println!("Name: {}", file.unwrap().file_name().to_string_lossy());
		
		for cap in re.captures_iter(&file.unwrap().file_name().to_string_lossy()) {
			let extracted_post_id = match cap.get(1) {
				Some (v) => v.as_str(),
				None => panic!("!!!!")
			};

			let fullfilename = match cap.get(0) {
				Some(v) => v.as_str().to_string(),
				None => panic!("!!!"),
			};
			//println!("{}", extracted_post_id);
			if post_id == extracted_post_id 
			{
				return Some((true, fullfilename));
			}
			continue
		}
	}

	return Some((false, "".to_owned()));
}

fn fix_image_references(original_note: String) -> (String, Vec<ImageFilename>)  {

	let re = Regex::new(r#"!\[([a-zA-Z0-9]{32}).(\S{3})]\(:/([a-zA-Z0-9]{32})\)"#).unwrap();
	let mut updated_note : String = original_note.clone();
	let mut image_information : Vec<ImageFilename> = Vec::new();
	for i in re.captures_iter(original_note.as_str()) {

		let extracted_full = match i.get(0) {
			Some (v) => v.as_str(),
			None => panic!("!!!!")
		};
		let extracted_original_file = match i.get(1) {
			Some (v) => v.as_str(),
			None => panic!("!!!!")
		};
		let extracted_extenstion = match i.get(2) {
			Some (v) => v.as_str(),
			None => panic!("!!!!")
		};
		let extracted_file = match i.get(3) {
			Some (v) => v.as_str(),
			None => panic!("!!!!")
		};

		image_information.push(ImageFilename {
			original_filename: extracted_original_file.to_string(),
			source_extension: extracted_extenstion.to_string(),
			source_filename : extracted_file.to_string(),
		});

		let to_be_replaced_with = format!("![{0}.{1}](/{2})", extracted_original_file,extracted_extenstion,  extracted_file);
		updated_note = updated_note.replace(extracted_full, to_be_replaced_with.as_str());
		
	}
	return (updated_note.to_string(), image_information);
}

fn does_contain_invalid_char(notetitle : String) -> bool
{
	let allowed_chars: Vec<char> = vec!['a','b','c','d','e' ,'f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z', '.', '-', ' ', '_'];
	let len_allowed_chars = allowed_chars.len() ;
	
	for c in notetitle.chars() {
		for i in 0..len_allowed_chars {
			if allowed_chars[i] == c {
				break;
			}
			if i == len_allowed_chars - 1{
				return  true;
			}
		}	
	}
	return false;
}
fn shadow_main() -> i32
{
	println!("joplin to hugo");
	println!("0.0.1");

	let c: Config = match get_config() {
		Some(v)=> v,
		None => return 0,
	};
	let conn = match Connection::open_with_flags(c.dbpath, OpenFlags::SQLITE_OPEN_READ_ONLY) {
		Ok(v) => v,
		Err(e) => {
			panic!("unable to access database: {}", e.to_string());  
		},
	};
	let to_parse_notest = get_notes(conn);
	let mut is_anything_dirty : bool = false;
	for note in to_parse_notest {
		let mut is_dirty : bool = false;
		let (updated_note, image_files) = fix_image_references(note.body);
		
		let (id_exists, dup_filename )=  match check_id(c.posts_path.clone(), note.id.clone()){
			Some(v) => v,
			None => return 1,
		};
		println!("note title: {0} (id : {1})", note.title, note.id);

		// check for characters which are not acceptable in 
		// file systems like * or % which can be in titles but
		// cannot be in filename.
		let title = note.title.replace(" ", "-");
		
		if does_contain_invalid_char(title.clone()) == true 
		{
			println!("\t[ERR] invalid character detected in the title");
			println!("\t[ERR] such character cannot be converted to filename");
			println!("\t[ERR] fix it to continue...");
			return -1;
		}

		let md_filename = format!("{0}-{1}.md",note.id, title );
		let dest_mdfile = format!("{0}{1}",c.posts_path, md_filename );

		// check for remote file 
		if std::path::Path::new(dest_mdfile.as_str()).exists() {
			println!("\t[WARN] file already exists..");
			// check the md5sum between the note and file
			let src_md5= md5::compute(updated_note.clone());
			let contents = fs::read_to_string(dest_mdfile.clone())
        			.expect("\t[ERR] Should have been able to read the file");
			let dst_md5  = md5::compute(contents);

			if src_md5 == dst_md5 {
				println!("\t[INFO] content are same.. skipping.");
			}
			else {
				is_dirty = true;
				is_anything_dirty = true;
				println!("\t[INFO] update post {0} to {1}", note.title, dest_mdfile.clone());
				let mut f = File::create(dest_mdfile).expect("\t[ERR] unable to destination .md file");
				f.write_all(updated_note.as_bytes()).expect("\r[ERR] unable to write to file");
			}
		}
		else {
			is_dirty = true;

			if id_exists == true && dup_filename != md_filename {
				eprintln!("[ERR] filename with same id exists..");
				eprintln!("[ERR] filename is : {}", dup_filename);
				return 1;
			}

			println!("\t[INFO] copy new post {0} to {1}", note.title, dest_mdfile.clone());
			let mut f = File::create(dest_mdfile).expect("\t[ERR] unable to open destination .md file for writing.");
			f.write_all(updated_note.as_bytes()).expect("\r[ERR] unable to write to file");
		}
		
		// now lets copy the images...
		if is_dirty == false
		{
			println!("\t[INFO] skipping image check as note not dirty...");
			continue;
		}
		else 
		{
			is_anything_dirty = true;
			for image in image_files{
			// check if this file already exists
				
				let source_image_file = format!("{0}{1}.{2}",c.image_src_path,image.source_filename.as_str(), image.source_extension.as_str());
				let dest_image_file = format!("{0}{1}", c.image_dest_path, image.source_filename.as_str());

				if std::path::Path::new(dest_image_file.as_str()).exists() 
				{
					let src_image_content = fs::read(source_image_file.as_str()).expect("\t[ERR] unable source image file !");
					let src_image_md5 = md5::compute(src_image_content);
							
					let dest_image_content = fs::read(dest_image_file.as_str()).expect("\t[ERR] unable to open destination ");
					let dest_image_md5 = md5::compute(dest_image_content);
					if src_image_md5 == dest_image_md5 {
						// no need to copy skip hashes are same
						println!("\t[INFO] same file.. skip");
					}
					else {
						// new file overwrite the content
						match fs::copy(source_image_file, dest_image_file) {
							Ok(_) => println!("\t [INFO] successfully overwritten {}", image.source_filename.as_str()),
							Err(e) => panic!("[ERR] unable to overwrite file: {}", &e),
						};
					}
				}
				else 
				{
					let mut source = match std::fs::File::open(&source_image_file) {
						Ok(v) => v,
						Err(e) => panic!("\t[ERR] unable to open source image file \n {}", e.to_string()),
					};
					let mut target = match std::fs::File::create(&dest_image_file) {
						Ok(v) => v,
						Err(e) => panic!("\t[ERR] unable to create the desintaion image file \n{}", e.to_string()),
					};
					match std::io::copy(&mut source, &mut target) {
						Ok(_) => println!("\t[INFO] copy successful {}",image.source_filename.as_str() ),
						Err(_) => panic!("\t[ERR] unable to copy file {}",image.source_filename.as_str() ),
					};
				}
			}
		}
	}
	
	if is_anything_dirty == true 
	{
		println!("[INFO] some content was changed");
		if c.auto_push == true 
		{
			println!("performing git autopush");
			if perform_auto_push(c.posts_doc_path) == false
			{
				return -1;
			}
		}
		else {
		    println!("skipping git autopush")
		}
	}
	else
	{
		println!("[INFO] no content change");

	}

	
	return 0;
}

fn get_notes(conn: Connection) -> Vec<TableNotes> {
	let mut data = conn.prepare("select * from notes",).unwrap();
	let mut r : Vec<TableNotes> = Vec::new();
	let list_notes = data.query_map([], |row| {
		Ok(TableNotes {
			id: row.get(0)?,
			parent_id : row.get(1)?,
			title:  row.get(2)?,
			body:  row.get(3)?,
			markup_language:  row.get(23)?,
		})
	}).unwrap();

	for item in list_notes {
		let i = item.unwrap();
		if i.body.starts_with("---\n") {
			r.push(i);
		}
	}
	r
}


fn perform_auto_push(upload_path: String) -> bool {
	
	println!("Enter comment for git push: ");
	let stdin = io::stdin();
	let value = stdin.lock().lines().next().unwrap().unwrap().trim().to_string();
	// let cmd = format!("git add . && git commit -a -m \"{}\" && git push", value);
	// let cmd = "git";
	// //let root = Path::new(&upload_path);
	//env::set_current_dir(&root);
	
	
	//println!("{:?}", root.display());

	//let s  = format!("git add . && git commit -a -m \"{}\" && git push", value);
	let output1 = match Command::new("git")
	.current_dir(upload_path.clone())
	.arg("add")
	.arg(".")
	.output(){
		Ok(v) => v,
		Err(..) => {
			eprintln!("unable to execute `git` command");
			return false;
		}
	};
	println!("=>{:?}",  &output1);
	
	let mut output2 = Command::new("git");
	output2.current_dir(upload_path.clone());
	output2.arg("git");
	output2.arg("commit");
	output2.arg("-a");
	output2.arg("-m");
	output2.arg(value.clone());
	output2.output();
	println!("==>{:?}",  output2);


	
	let output3 = Command::new("git")
	.current_dir(upload_path)
	.arg("push")
	.output()
	.expect("Failed to execute command");
	println!("===>{:?}",  output3);

	return true;
}